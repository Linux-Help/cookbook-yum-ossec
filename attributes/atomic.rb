default['yum']['atomic']['repositoryid'] = 'atomic'
default['yum']['atomic']['enabled'] = true
default['yum']['atomic']['managed'] = true
default['yum']['atomic']['gpgkey'] = 'https://www.atomicorp.com/RPM-GPG-KEY.atomicorp.txt https://www.atomicorp.com/RPM-GPG-KEY.art.txt'
default['yum']['atomic']['gpgcheck'] = true
default['yum']['atomic']['priority'] = 5

case node['platform_family']
when 'rhel'
    default['yum']['atomic']['description'] = 'CentOS / Red Hat Enterprise Linux $releasever - atomicrocketturtle.com'
    default['yum']['atomic']['mirrorlist'] = "http://updates.atomicorp.com/channels/mirrorlist/atomic/centos-$releasever-$basearch"
end

