name             'yum-ossec'
maintainer       'Eric Renfro'
maintainer_email 'psi-jack@linux-help.org'
license          'Apache 2.0'
description      'Installs and configures the Official OSSEC Repository'
long_description ''
version          '0.1.1'
issues_url       'http://gogs.home.ld/Linux-Help/cookbook-yum-ossec/issues'
source_url       'http://gogs.home.ld/Linux-Help/cookbook-yum-ossec'

%w{ centos redhat oracle scientific }.each do |os|
	supports os, '>= 5.0.0'
end

depends 'yum', '>= 3.2'

